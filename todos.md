# Home Screen (Steffen)
- Device drehen 

# Live-Identifikation (Steffen)
- Standort abgreifen

# Detailseite Pflanzenerkennung (Daniel)

More: Bei More eine richtige Liste 
	- Tabelle: Name, Confidence 
	- ggf. schon richtigen Namen anzeigen (erfordert prefetching)

- Bild-Darstellung verschönern 

- History durch Home ersetzen 

- Platz wechseln von More & Home 

- Sharebutton implementieren 

- Device drehen

# Historie (Artem) 

- GUI wie die Mocks implementieren. 
- Device drehen

# Detailseite-Historie (Artem) 

- GUI wie die Mocks implementieren. 
- Device drehen

# Settings (Steffen)

- History speichern (an / aus)

# Misc 

- Darkmode macht Plantfinder Logo kapput (Steffen)

# Ausblick 

Onboarding Screen
