package com.mse2324.plantfinder

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mse2324.plantfinder.http.PlantInfo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SearchViewModel : ViewModel() {

    //val plant: Plant = Plant("Hibiscus botanicus botanicus", "12.12.12 20:00",imageResourceId = R.drawable.capturepilz)

    var plantInfo: PlantInfo = PlantInfo()
    private val plant_info_ = MutableStateFlow(plantInfo)

    private val _searchText = MutableStateFlow("")
    var searchText: StateFlow<String> = _searchText.asStateFlow()


    private val _isSearching = MutableStateFlow(false)
    val isSearching: StateFlow<Boolean> = _isSearching.asStateFlow()


    fun setPlantDetails(plantInfo: PlantInfo) {
        viewModelScope.launch {
            plant_info_.value = plantInfo
        }
    }

    fun onSearchTextChange(text: String) {
        _searchText.value = text
        println("onSearchTextChange")
    }

    fun onToogleSearch() {
        println("onToogleSearch")
        //_isSearching.value = !_isSearching.value
//        if (!_isSearching.value) {
//            onSearchTextChange("")
//        }
    }
}