package com.mse2324.plantfinder.http

import android.location.Location
import android.os.Build
import android.util.Log
import androidx.compose.ui.text.capitalize
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.get
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol
import io.ktor.http.path
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class WikiHttpClient {

    suspend fun getPlantByScientificName(sciName: String): PlantInfo {
        val client = HttpClient(CIO) {
            install(ContentNegotiation) {
                json(Json{
                    ignoreUnknownKeys = true
                })
            }
        }
        val pageTitle = fetchPageTitle(sciName, client)
        if (pageTitle == "") return PlantInfo(
            "",
            "",
            Thumbnail("", -1, -1),
            sciName = sciName,
            url = ""
        )
        val res = fetchPlantInfo(pageTitle, client)
        res.sciName =
            sciName.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
                .replace('_', ' ')
        res.url = "https://en.wikipedia.org/wiki/" + pageTitle
        return res

    }

    private suspend fun fetchPageTitle(sciName: String, client: HttpClient): String {

        val searchResult: JsonArray = client.get {
            url {
                protocol = URLProtocol.HTTPS
                host = "en.wikipedia.org"
                path("w/api.php")
                parameters.append("action", "opensearch")
                parameters.append("search", sciName)
                parameters.append("limit", "10")
                parameters.append("format", "json")
            }
        }.body()

        val urls = searchResult[3]

        if (urls !is JsonArray) {
            return ""
        }

        val bestFittingTitle = URLBuilder(urls[0].toString().trim('"'))
            .pathSegments
            .last()
        println(bestFittingTitle)

        return bestFittingTitle.toString()

    }

    private suspend fun fetchPlantInfo(pageTitle: String, client: HttpClient): PlantInfo {
        val extractsResponse: ExtractsBody =
            client.get {
                url {
                    protocol = URLProtocol.HTTPS
                    host = "en.wikipedia.org"
                    path("w/api.php")
                    parameters.append("action", "query")
                    parameters.append("prop", "extracts|pageimages")
                    parameters.append("exintro", "1")
                    parameters.append("explaintext", "1")
                    parameters.append("format", "json")
                    parameters.append("formatversion", "2")
                    parameters.append("redirects", "1")
                    parameters.append("titles", pageTitle)
                    parameters.append("piprop", "thumbnail")
                    parameters.append("pithumbsize", "400")
                }
            }.body()
        return extractsResponse.query.pages[0]
    }

}
@Serializable
data class ExtractsBody(
    val query: Query
)

@Serializable
data class Query(
    val pages: List<Page>
)

@Serializable
data class Page(
    val extract: String = "",
    val title: String = "",
    val thumbnail: Thumbnail = Thumbnail("", -1, -1),
    var timestamp: Long =  System.currentTimeMillis(),
    var url: String = "",
    var sciName: String = "",
    var position: Position = Position(0.0,0.0),
    var granted_geo: Boolean = false
)

@Serializable
data class Position(
    var latitude: Double = 0.0,
    var longitude: Double = 0.0
)

@Serializable
data class Thumbnail(
    val source: String,
    val width: Int,
    val height: Int
)

typealias PlantInfo = Page


/*
Short-Description:

Url:
https://en.wikipedia.org/w/api.php?action=query&prop=extracts|pageimages&exintro=1
&explaintext=1&titles=Helianthus_annuus&format=json&formatversion=2&redirects=1&piprop=thumbnail&pithumbsize=300
Response:
{
  "batchcomplete": true,
  "query": {
    "normalized": [
      {
        "fromencoded": false,
        "from": "Helianthus_annuus",
        "to": "Helianthus annuus"
      }
    ],
    "redirects": [
      {
        "from": "Helianthus annuus",
        "to": "Common sunflower"
      }
    ],
    "pages": [
      {
        "pageid": 57622,
        "ns": 0,
        "title": "Common sunflower",
        "extract": "The common sunflower (Helianthus annuus) is a species of large annual forb of the genus Helianthus. It is commonly grown as a crop for its edible oily seeds. Apart from cooking oil  production, it is also used as livestock forage (as a meal or a silage plant), as bird food, in some industrial applications, and as an ornamental in domestic gardens. Wild H. annuus is a widely branched annual plant with many flower heads. The domestic sunflower, however, often possesses only a single large inflorescence (flower head) atop an unbranched stem.\nHelianthus annuus (common sunflower) belong to the dicotyledonous category, distinguishing them from monocots. As dicots, sunflowers possess embryos with two veins, known as cotyledons, a characteristic feature that sets them apart. The veins in their leaves exhibit a net-like pattern, in contrast to monocots, which typically display parallel leaf veins."
      }
    ]
  }
}
*/