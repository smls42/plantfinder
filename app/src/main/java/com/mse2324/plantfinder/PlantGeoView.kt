package com.mse2324.plantfinder

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.bumptech.glide.integration.compose.placeholder
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import com.mse2324.plantfinder.http.PlantInfo
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import com.mse2324.plantfinder.model.AppViewModel
import java.util.Date

@OptIn(ExperimentalGlideComposeApi::class)
@SuppressLint("StateFlowValueCalledInComposition")
@Composable
fun PlantgeoView(navController: NavController, searchViewModel: SearchViewModel, appViewModel: AppViewModel) {

    val plantInfo: PlantInfo = searchViewModel.plantInfo

    val localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(plantInfo.timestamp), ZoneId.systemDefault())

    val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
    val formattedDateTime = localDateTime.format(formatter)
    val pos =  appViewModel.position
    //val msg = navController.currentBackStackEntry?.savedStateHandle?.get<String>("msg")?: "nothing"
    //val plant = Plant("Hibiscus botanicus botanicus", "12.12.12 20:00", imageResourceId = R.drawable.capturepilz)

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            //verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            GlideImage(model = plantInfo.thumbnail.source, contentDescription = "",
                modifier = Modifier
                    .size(190.dp)
                    .clip(RoundedCornerShape(10.dp))
                    .padding(8.dp),
                failure = placeholder(R.drawable.baseline_local_florist_24)
            )

            Text(modifier = Modifier.padding(10.dp),
                text = plantInfo.title,
                fontWeight = FontWeight.Bold,
            )
            Divider(color = Color.Black, thickness = 1.dp)
            Text(modifier = Modifier.padding(10.dp), text = formattedDateTime)


            if (pos.latitude.toInt() != 0.0.toInt() || pos.longitude.toInt() != 0.0.toInt()) {
                val cameraPositionState = rememberCameraPositionState {
                    position = CameraPosition.fromLatLngZoom(LatLng(pos.latitude,pos.longitude), 10f)
                }
                GoogleMap(
                    modifier = Modifier.fillMaxSize(),
                    cameraPositionState = cameraPositionState
                ) {
                    Marker(
                        state = MarkerState(position = LatLng(pos.latitude,pos.longitude) ),
                        title = "found here",
                        snippet = ""
                    )
                }
            } else {
                Text(modifier = Modifier.padding(10.dp), text = "No geoposition available for this plant")
            }
        }
    }
}
