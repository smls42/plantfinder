package com.mse2324.plantfinder

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import android.Manifest
import android.app.Activity
import android.content.pm.ActivityInfo
import android.net.Uri
import android.util.Log
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.permissions.isGranted
import com.google.mlkit.common.model.LocalModel
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.label.ImageLabeling
import com.google.mlkit.vision.label.custom.CustomImageLabelerOptions
import com.mse2324.plantfinder.http.PlantInfo
import com.mse2324.plantfinder.model.AppViewModel
import com.mse2324.plantfinder.model.SettingsViewModel
import com.mse2324.plantfinder.ui.theme.PlantfinderTheme
import java.util.concurrent.Executors

/* refs:
https://github.com/jordan-jakisa/CameraXSample
 */


class MainActivity : ComponentActivity() {

    companion object MainActivity {
        const val NAV_MAIN = "navMain"
        const val NAV_LIVEIDENT = "navIdentify"
        const val NAV_HISTORY = "navHistory"
        const val NAV_SETTINGS = "navSettings"
        const val NAV_DETAIL = "navDetail"
        const val IS_DEBUG = false
        const val NAV_PLANTGEOVIEW = "PlantgeoView"
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContent {
            PlantfinderTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Nav()
                }
            }
        }
    }
}

@Composable
fun Nav(
    appViewModel: AppViewModel = viewModel(),
    settingsViewModel: SettingsViewModel = viewModel(),
    viewModel: SearchViewModel = SearchViewModel()
) {
    val navController = rememberNavController()
    val appContext = LocalContext.current

    (appContext as? Activity)?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    LaunchedEffect(Unit){
        appViewModel.loadHistoryFromDisk(appContext)
        settingsViewModel.loadSettingsFromDisk(appContext)
        Log.d("MainActivity", "Loaded History Entries: ${appViewModel.sessionHistory.size}")
        for (entry in appViewModel.sessionHistory) {
            Log.d("MainActivity", "title: ${entry.title}, timestamp: ${entry.timestamp} url: ${entry.thumbnail}" )
        }
    }

    NavHost(navController = navController, startDestination = MainActivity.NAV_MAIN) {
        composable(MainActivity.NAV_MAIN) { Main(navController, appViewModel) }
        composable(MainActivity.NAV_LIVEIDENT) { LiveIdentView(navController, appViewModel) }
        composable(MainActivity.NAV_DETAIL) { PlantDetailView(navController, appViewModel, settingsViewModel) }
        composable(MainActivity.NAV_SETTINGS) { SettingsView(settingsViewModel) }

        composable(MainActivity.NAV_HISTORY) { HistorieView(navController, viewModel, appViewModel) }
        composable(MainActivity.NAV_PLANTGEOVIEW) { PlantgeoView(navController, viewModel, appViewModel) }
    }

    DisposableEffect(Unit) {
       onDispose {
           appViewModel.saveHistoryToDisk(appContext)
           settingsViewModel.saveSettingsToDisk(appContext)
       }
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun Main(
    navController: NavController,
    viewModel: AppViewModel
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        val executor = remember { Executors.newSingleThreadExecutor() }

        val localModel = LocalModel.Builder()
            .setAssetFilePath("plants.tflite")
            .build()

        val customImageLabelerOptions = CustomImageLabelerOptions.Builder(localModel)
            .setConfidenceThreshold(0.5f)
            .setMaxResultCount(5)
            .setExecutor(executor)
            .build()

        val labeler =
            remember { ImageLabeling.getClient(customImageLabelerOptions) }

        val context = LocalContext.current

        var isLoading by remember { mutableStateOf(false) }

        val launcher = rememberLauncherForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            if (uri != null) {
                isLoading = true
                val img = InputImage.fromFilePath(context, uri)
                labeler.process(img).addOnCompleteListener {task ->
                    isLoading = false
                    viewModel.identResults = task.result
                    viewModel.preventDetailNavigation = false
                    viewModel.plantImageProcessed = true
                }
            }
        }

        Box(modifier = Modifier.fillMaxSize()) {
            Home(navController, launcher)

            if (isLoading) {
                Box(
                    modifier = Modifier
                        .size(200.dp, 100.dp)
                        .fillMaxWidth()
                        .background(
                            color = MaterialTheme.colorScheme.background,
                            shape = RoundedCornerShape(8.dp)
                        )
                        .align(Alignment.Center)
                ) {

                    CircularProgressIndicator(
                        modifier = Modifier
                            .size(50.dp)
                            .align(Alignment.Center)
                    )
                }

            }

            if (!viewModel.plantImageProcessed || viewModel.preventDetailNavigation) {
                return
            }
            if (viewModel.identResults.isEmpty() || viewModel.identResults[0].text == "None") {
                NoMatchDialog(viewModel)
            } else {
                viewModel.preventDetailNavigation = true
                navController.navigate(MainActivity.NAV_DETAIL)
            }

        }


    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun Home(
    navController: NavController,
    launcher: ManagedActivityResultLauncher<PickVisualMediaRequest, Uri?>
) {
    val mapPermissionLoc1State = rememberPermissionState(Manifest.permission.ACCESS_FINE_LOCATION) // wifi
    val mapPermissionLoc2State = rememberPermissionState(Manifest.permission.ACCESS_COARSE_LOCATION) // gps

    val cameraPermissionState = rememberPermissionState(Manifest.permission.CAMERA)
    var showPermissionPopup by remember {
        mutableStateOf(false)
    }

    var showPermissionPopup2 by remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier
            .padding(vertical = 20.dp)
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Logo()
        Spacer(modifier = Modifier.height(100.dp))
        if ( !mapPermissionLoc1State.status.isGranted && showPermissionPopup2) {

            AlertDialog(
                title = { Text(text = "Grant Permissions") },
                text = { Text(text = "Plant identification requires geolocation ") },
                onDismissRequest = { showPermissionPopup2 = false },
                confirmButton = {
                    Button(onClick = {
                        //mapPermissionLoc2State.launchPermissionRequest()
                        mapPermissionLoc1State.launchPermissionRequest()

                        showPermissionPopup2 = false
                    }) {
                        Text("Grant")
                    }
                }, dismissButton = {
                    Button(onClick = {
                        showPermissionPopup2 = false
                    }) {
                        Text("Dismiss")
                    }
                }
            )


        }
        if (!cameraPermissionState.status.isGranted && showPermissionPopup) {
            // TODO Internationalize
            AlertDialog(
                title = { Text(text = "Grant Permissions") },
                text = { Text(text = "Plant identification requires camera access") },
                onDismissRequest = { showPermissionPopup = false },
                confirmButton = {
                    Button(onClick = {

                        cameraPermissionState.launchPermissionRequest()
                        showPermissionPopup = false
                    }) {
                        Text("Grant")
                    }
                }, dismissButton = {
                    Button(onClick = {
                        showPermissionPopup = false
                    }) {
                        Text("Dismiss")
                    }
                }
            )
        }
        HomescreenButton("Identify") {
            if (cameraPermissionState.status.isGranted) {
                navController.navigate(MainActivity.NAV_LIVEIDENT)
            } else {
                showPermissionPopup = true
                showPermissionPopup2 = true
            }
        }
        HomescreenButton("Upload") {
            if (mapPermissionLoc1State.status.isGranted) {
                launcher.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
            } else {
                showPermissionPopup2 = true
            }
        }
        HomescreenButton("History") {
            navController.navigate(MainActivity.NAV_HISTORY)
        }
        HomescreenButton("Settings") {
            navController.navigate(MainActivity.NAV_SETTINGS)
        }
    }
}

@Composable
fun Logo() {
    // Static Image with Rounded Corners
    Image(
        painter = painterResource(id = R.drawable.plantfinder), // Replace with your image resource
        contentDescription = null, // Provide a content description if needed
    )
}

@Composable
private fun HomescreenButton(
    buttonText: String,
    onClick: () -> Unit
) {
    Button(
        onClick = onClick,
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth(0.9f)
            .padding(8.dp)
    ) {
        Text(buttonText)
    }
}

