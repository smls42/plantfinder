package com.mse2324.plantfinder

import androidx.annotation.OptIn
import androidx.camera.core.CameraSelector
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.view.LifecycleCameraController
import androidx.camera.view.PreviewView
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.window.Dialog
import androidx.navigation.NavController
import com.google.mlkit.common.model.LocalModel
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.label.ImageLabel
import com.google.mlkit.vision.label.ImageLabeling
import com.google.mlkit.vision.label.custom.CustomImageLabelerOptions
import com.mse2324.plantfinder.http.PlantInfo
import com.mse2324.plantfinder.model.AppViewModel
import java.util.concurrent.Executors

@OptIn(ExperimentalGetImage::class)
@Composable
fun LiveIdentView(
    navController: NavController,
    viewModel: AppViewModel
) {

    val context = LocalContext.current
    val previewView: PreviewView = remember { PreviewView(context) }
    val cameraController = remember { LifecycleCameraController(context) }
    val lifecycleOwner = LocalLifecycleOwner.current
    cameraController.bindToLifecycle(lifecycleOwner)
    cameraController.cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
    previewView.controller = cameraController

    val executor = remember { Executors.newSingleThreadExecutor() }

    val localModel = LocalModel.Builder()
        .setAssetFilePath("plants.tflite")
        .build()

    val customImageLabelerOptions = CustomImageLabelerOptions.Builder(localModel)
        .setConfidenceThreshold(0.5f)
        .setMaxResultCount(5)
        .setExecutor(executor)
        .build()

    val labeler =
        remember { ImageLabeling.getClient(customImageLabelerOptions) }

    var isLoading by remember { mutableStateOf(false) }

    Box(modifier = Modifier.fillMaxSize()) {
        AndroidView(factory = { previewView }, modifier = Modifier.fillMaxSize())
        if (isLoading) {
            CircularProgressIndicator(
                modifier = Modifier
                    .size(50.dp)
                    .align(Alignment.Center)
            )
        } else {
            Row(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
            ){
                Button(
                    modifier = Modifier
                        .padding(16.dp),
                    onClick = { navController.navigate(MainActivity.NAV_MAIN) }) {
                    Text("Home")
                }
                LiveIdentButton(
                   modifier = Modifier
                       .padding(16.dp)
                ) {
                    isLoading = true
                    cameraController.setImageAnalysisAnalyzer(executor) { imageProxy ->
                        imageProxy.image?.let { image ->
                            val img = InputImage.fromMediaImage(
                                image,
                                imageProxy.imageInfo.rotationDegrees
                            )

                            labeler.process(img).addOnCompleteListener { task ->

                                isLoading = false
                                viewModel.identResults = task.result
                                viewModel.preventDetailNavigation = false
                                viewModel.plantImageProcessed = true

                                cameraController.clearImageAnalysisAnalyzer()
                                imageProxy.close()
                            }
                        }
                    }
                }
                if (MainActivity.IS_DEBUG) {
                    Button(
                        modifier = Modifier
                            .padding(16.dp),
                        onClick = {
                            val imageLabels = listOf(
                                ImageLabel("helianthus_annuus", 0.99f, 0),
                                ImageLabel("narcissus_pseudonarcissus", 0.91f, 0),
                            )
                            viewModel.identResults = imageLabels
                            navController.navigate(MainActivity.NAV_DETAIL)
                        }) {
                        Text("Debug")
                    }
                }

            }
            if (!viewModel.plantImageProcessed || viewModel.preventDetailNavigation) {
                return
            }
            if (viewModel.identResults.isEmpty() || viewModel.identResults[0].text == "None") {
                NoMatchDialog(viewModel)
            } else {
                viewModel.preventDetailNavigation = true
                navController.navigate(MainActivity.NAV_DETAIL)
            }
        }
    }
}


@Composable
fun NoMatchDialog(
    viewModel: AppViewModel
) {
    Dialog(onDismissRequest = { viewModel.resetPlantIdentification() }) {
        Card(modifier = Modifier.fillMaxWidth(0.8f)) {
            Text(
                text = stringResource(id = R.string.no_match),
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .padding(top = 16.dp),
                style = MaterialTheme.typography.bodySmall
            )
            Button(
                onClick = { viewModel.resetPlantIdentification() },
                modifier = Modifier
                    .align(Alignment.End)
                    .padding(horizontal = 16.dp, vertical = 16.dp)
            ) {
                Text(text = "Ok")
            }
        }
    }
}


@OptIn(ExperimentalGetImage::class)
@Composable
fun LiveIdentButton(
    modifier: Modifier,
    onClick: () -> Unit
) {
    Button(
        onClick = onClick,
        modifier = modifier
    ) {
        Text("Identify")
    }
}
