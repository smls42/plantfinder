package com.mse2324.plantfinder
import android.annotation.SuppressLint
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.SearchBar
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.isTraversalGroup
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.bumptech.glide.integration.compose.placeholder
import com.mse2324.plantfinder.http.PlantInfo
import com.mse2324.plantfinder.model.AppViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.Date

const val NAV_PLANTGEOVIEW = "PlantgeoView"

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HistorieView(navController: NavController, viewModel: SearchViewModel, appViewModel: AppViewModel) {

    val plants = appViewModel.sessionHistory.sortedBy { -it.timestamp }

    //val plants by viewModel.plantList.collectAsState()

    var text by rememberSaveable { mutableStateOf("") }
    var active by rememberSaveable { mutableStateOf(false) }
    var search_queries = remember {
        mutableStateListOf<String>()
    }

    Scaffold(
        floatingActionButton = {
            FloatingActionButton(onClick = {
                appViewModel.sessionHistory = listOf()
            }) {
                Icon(imageVector = Icons.Default.Delete, contentDescription = "Clear Search History")
            }
        }
    ) {Box(
        Modifier
            .padding(it)
            .fillMaxSize()
            .semantics { isTraversalGroup = true }){

        Column (modifier = Modifier.padding(all = 14.dp)
            //.fillMaxSize()
        ) {
            SearchBar(
                query = text,
                onQueryChange = {text = it} ,
                onSearch = {search_queries.add(text)
                    active = false
                },
                active = active,
                onActiveChange = {active = it},
                placeholder = { Text("Search") },
                leadingIcon = { Icon(Icons.Default.Search, contentDescription = null) },
                trailingIcon = {
                    if(active) {
                        Icon(modifier = Modifier.clickable{
                            if (text.isNotEmpty()) {
                                text = ""
                            } else {
                                active = false
                            }
                        },
                            imageVector = Icons.Default.Close,
                            contentDescription = "Close Icon")
                    }
                }, modifier = Modifier.fillMaxWidth()
            ) {
                search_queries.forEach{
                    Row(modifier = Modifier
                        .padding(all = 14.dp)
                        .clickable(onClick = {
                            if (!search_queries.contains(it)) { // or !it.isEmpty()
                                search_queries.add(it.toString())
                            }
                            active = false

                        })
                    ) {
                        Icon(modifier = Modifier.padding(end = 10.dp),
                            imageVector = Icons.Default.Refresh,
                            contentDescription = "Star Icon" )
                        Text(text = it)
                    }
                }
            }

            var plantList = plants.filter { plant ->
                plant.title.uppercase().contains(text.trim().uppercase())
            }

            if (plants.isNotEmpty() and plantList.isEmpty()) {
                Text(text = "No plants found in history", modifier = Modifier.padding(16.dp))
            }

            PlantGrid(plantList, navController, viewModel )


            if (plants.isEmpty()) {
                Text(text = "History is empty", modifier = Modifier.padding(16.dp))
            }
        }
    }

    }


}



// PlantGrid(plantList = plants)

@Composable
fun PlantGrid(plantList: List<PlantInfo>, navController: NavController, viewModel: SearchViewModel) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(1)
    ) {
        items(plantList) { plant ->
            PlantItem(plant = plant, navController, viewModel)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalGlideComposeApi::class)
@Composable
fun PlantItem(plant: PlantInfo, navController: NavController, viewModel: SearchViewModel) {

    val backgroundColor = Color.White
    val title = plant.title

    Box(
        modifier = Modifier
            .padding(1.dp)
            .border(2.dp, Color.White)
            .fillMaxWidth()
            .aspectRatio(2f)
            .background(backgroundColor)

    ) {
            Column(
                modifier = Modifier
                    .clickable(
                        onClick = {
                            navController.navigate(NAV_PLANTGEOVIEW)
                            //navController.previousBackStackEntry?.savedStateHandle?.set("msg", title)
                            //navController.currentBackStackEntry?.savedStateHandle?.set("message", title)

                            val _searchText = MutableStateFlow(title)
                            viewModel.searchText = _searchText

                            viewModel.setPlantDetails(plant)
                            viewModel.plantInfo = plant
                        }
                    )
                    .fillMaxSize()
                    .height(5.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Row (
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,

                ) {
//                    plant.imageResourceId?.let { imageResId ->
//                        Image(painter = painterResource(id = imageResId), contentDescription = null,
//                            modifier = Modifier
//                                .size(120.dp)
//                                .clip(RoundedCornerShape(10.dp))
//                                .padding(8.dp))
//                    }

                    plant.thumbnail.source?.let { source ->
                        println("source " + source)
                        GlideImage(model = source, contentDescription = "",
                            modifier = Modifier
                                .size(120.dp)
                                .clip(RoundedCornerShape(10.dp))
                                .padding(8.dp),
                            failure = placeholder(R.drawable.baseline_local_florist_24)
                        )
                    }

                    Text(modifier = Modifier.fillMaxWidth(),
                        text = title,
                        fontWeight = FontWeight.Bold,
                    )
                }

                val localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(plant.timestamp), ZoneId.systemDefault())

                val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
                val formattedDateTime = localDateTime.format(formatter)


                Divider(color = Color.Black, thickness = 1.dp)
                Text(text = formattedDateTime, fontWeight = FontWeight.Bold, modifier = Modifier.padding(10.dp) )

            }
    }
}


//##########################

@OptIn(ExperimentalMaterial3Api::class)
@Preview(showBackground = true)
@Composable
fun NoteGridPreview() {

}