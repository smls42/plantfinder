package com.mse2324.plantfinder

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.bumptech.glide.integration.compose.placeholder
import com.google.mlkit.vision.label.ImageLabel
import com.mse2324.plantfinder.model.AppViewModel
import com.mse2324.plantfinder.model.SettingsViewModel

import android.content.Context
import android.content.pm.PackageManager
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Share
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.navigation.NavController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.launch

@SuppressLint("PermissionLaunchedDuringComposition")
@OptIn(ExperimentalMaterial3Api::class, ExperimentalGlideComposeApi::class,
    ExperimentalPermissionsApi::class
)
@Composable
fun PlantDetailView(
    navController: NavController,
    viewModel: AppViewModel = viewModel(),
    settingsViewModel: SettingsViewModel = viewModel()
) {
    val identResults = viewModel.identResults
    val plantInfo = viewModel.plantDetail

    var showDetails by remember { mutableStateOf(true) }

    val sheetState = rememberModalBottomSheetState()
    val scope = rememberCoroutineScope()
    var showBottomSheet by remember { mutableStateOf(false) }
    val context = LocalContext.current

    //LaunchedEffect(identResults) {
    //for (result in identResults.sortedBy {
     //       it.confidence
     //   }) {
    //
      //      var locationProvider = LocationServices.getFusedLocationProviderClient(context)
      //      viewModel.loadMatchInfo(result, settingsViewModel.settings.saveHistory, locationProvider)
      //  }
    //}
    var locationProvider = LocationServices.getFusedLocationProviderClient(context)

    LaunchedEffect(identResults) {
        val plantName = identResults[0].text
        viewModel.setPlantDetails(plantName, settingsViewModel.settings.saveHistory, locationProvider )
    }



    //viewModel.setPlantDetails(result, settingsViewModel.settings.saveHistory, locationProvider)
    // todo war früher
    // var locationProvider = LocationServices.getFusedLocationProviderClient(context)
    //            viewModel.loadMatchInfo(result, settingsViewModel.settings.saveHistory, locationProvider)

    Scaffold(bottomBar = {
        BottomAppBar(
            containerColor = Color.White
        ) {
            Column(
                verticalArrangement = Arrangement.Center
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly
                ) {

                    Button(onClick = { navController.navigate(MainActivity.NAV_MAIN) }) {
                        Text("Home")
                    }

                    Button(onClick = { showBottomSheet = true }) {
                        Text(text = "All possible plants")
                    }

                    val context = LocalContext.current
                    Button(onClick = {

                        share(
                            context,
                            "Check out this plant! I could identify it with Plantfinder: \"" + plantInfo.value.title + "\" " +
                                    "You can read more about it here: " + plantInfo.value.url
                        )
                    }) {
                        Icon(
                            Icons.Rounded.Share, contentDescription = "Share"
                        )
                    }

                }
            }
        }
    }) { innerPadding ->

        Column(
            modifier = Modifier
                .padding(innerPadding),
            verticalArrangement = Arrangement.spacedBy(8.dp),

            ) {

            GlideImage(
                model = plantInfo.value.thumbnail.source,
                contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(300.dp),
                failure = placeholder(R.drawable.baseline_local_florist_24)
            )

            Column(
                modifier = Modifier
                    .padding(12.dp)
            ) {
                Text(
                    plantInfo.value.title,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.headlineMedium
                )

                Text(
                    plantInfo.value.sciName, textAlign = TextAlign.Center
                )

                Divider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(2.dp)
                        .background(Color.Black)
                )

                Text(
                    text = "Description", style = MaterialTheme.typography.headlineSmall,
                    modifier = Modifier.padding(vertical = 6.dp)
                )

                Column(
                    modifier = Modifier.fillMaxHeight()
                ) {
                    Column(
                        modifier = Modifier
                            .verticalScroll(rememberScrollState())
                            .weight(2f)
                    ) {
                        Text(
                            text = plantInfo.value.extract
                        )
                    }


                }
            }

        }


        if (showBottomSheet) {

            ModalBottomSheet(
                onDismissRequest = {
                    showBottomSheet = false
                }, sheetState = sheetState
            ) {
                Column(
                    Modifier.padding(innerPadding)
                ) {
                    for (identResult in identResults) {
                        Row(modifier = Modifier
                            .clickable {
                                showDetails = true
                                //todo check from master
                                // viewModel.setPlantDetails(identResult.text, settingsViewModel.settings.saveHistory)
                                viewModel.setPlantDetails(identResult.text, settingsViewModel.settings.saveHistory, locationProvider)
                                scope
                                    .launch { sheetState.hide() }
                                    .invokeOnCompletion {
                                        if (!sheetState.isVisible) {
                                            showBottomSheet = false
                                        }
                                    }
                            }
                            .padding(8.dp)) {
                            Text("${identResult.text}, ${identResult.confidence}")
                        }
                    }
                }
            }
        }
    }
}

fun share(context: Context, text: String) {

    val sendIntent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, text)
        type = "text/plain"
    }

    val shareIntent = Intent.createChooser(sendIntent, null)
    startActivity(context, shareIntent, null)
}

@Preview
@Composable
fun Preview() {
    val viewModel = AppViewModel()
    val navController = NavController(context = LocalContext.current)
    val imageLabels = listOf(
        ImageLabel("Poop", 0.99f, 0),
        ImageLabel("narcissus_pseudonarcissus", 0.91f, 0),
    )

    viewModel.identResults = imageLabels
    PlantDetailView(navController, viewModel)
}
