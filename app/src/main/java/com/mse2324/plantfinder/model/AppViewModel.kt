package com.mse2324.plantfinder.model

import android.annotation.SuppressLint
import android.content.Context
import android.health.connect.datatypes.ExerciseRoute
import android.location.Location
import android.location.LocationProvider
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.CancellationTokenSource
import com.google.android.gms.tasks.Task
import com.google.mlkit.vision.label.ImageLabel
import com.mse2324.plantfinder.http.PlantInfo
import com.mse2324.plantfinder.http.Position
import com.mse2324.plantfinder.http.WikiHttpClient
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

class AppViewModel(
    private val httpService: WikiHttpClient = WikiHttpClient(),
) : ViewModel() {

    private val historyFile = "history.json"

    private var plantInfoCache = mutableMapOf<String, PlantInfo>()

    private var _plantInfo = mutableStateOf(PlantInfo())
    val plantDetail = _plantInfo

    var plantImageProcessed by mutableStateOf(false)

    var identResults by mutableStateOf(listOf<ImageLabel>())

    var preventDetailNavigation by mutableStateOf(false)

    //todo check mein variante : var sessionHistory = mutableListOf<PlantInfo>()
    var sessionHistory by mutableStateOf(listOf<PlantInfo>())
    var position: Position = Position(0.0,0.0)

    @SuppressLint("MissingPermission")
    fun setPlantDetails(plantName: String, addToHistory: Boolean, fusedLocationClient: FusedLocationProviderClient) {

        val res = fusedLocationClient.getLastLocation()

        runBlocking {
            val resultDeferred: Deferred<Task<Location>> = async {
                res.addOnCompleteListener{

                    position.longitude = it.result.longitude
                    position.latitude = it.result.latitude

                }
            }
        }

        val position: Position
        position = Position(0.0,0.0)



//        val plantInfoCached = plantInfoCache[plantName]
//
//        //V todo add vlt position
//        plantInfoCached.position = position

        if (plantInfoCache[plantName] != null) {
            val plantInfo = plantInfoCache[plantName]!!
            _plantInfo.value = plantInfo.copy(
                timestamp = System.currentTimeMillis()
            )
            if (addToHistory) {
                sessionHistory = sessionHistory + _plantInfo.value
            }
        } else {
            viewModelScope.launch {
                try {
                    val plantInfo = httpService.getPlantByScientificName(plantName)
                    plantInfoCache[plantName] = plantInfo
                    if (addToHistory) {
                        sessionHistory = sessionHistory + plantInfo
                    }
                    _plantInfo.value = plantInfo
                } catch (e: Exception) {
                    Log.e("HttpClient", "${e}, Message: ${e.message}")
                    _plantInfo.value = PlantInfo(
                        title = plantName,
                        extract = "Plant data not available, please check your internet connection"
                    )
                }
            }
        }
    }

    fun resetPlantIdentification() {
        this.plantImageProcessed = false
        this.identResults = listOf()
    }

    fun saveHistoryToDisk(context: Context) {
        val json = Json.encodeToString(sessionHistory)
        val historyPath = File(context.filesDir, historyFile)
        historyPath.writeText(json)
    }

    fun loadHistoryFromDisk(context: Context) {
        val historyPath = File(context.filesDir, historyFile)
        if (!historyPath.exists()) return
        val json = historyPath.readText()
        sessionHistory = Json.decodeFromString<MutableList<PlantInfo>>(json)
    }

}