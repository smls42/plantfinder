package com.mse2324.plantfinder.model

import android.content.Context
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

@Serializable
class AppSettings(
    var saveHistory: Boolean
)

class SettingsViewModel(): ViewModel() {

    private val settingsFile = "settings.json"

    var settings by mutableStateOf(AppSettings(true))

    fun saveSettingsToDisk(context: Context) {
        val json = Json.encodeToString(settings)
        val historyPath = File(context.filesDir, settingsFile)
        historyPath.writeText(json)
    }

    fun loadSettingsFromDisk(context: Context) {
        val historyPath = File(context.filesDir, settingsFile)
        if (!historyPath.exists()) return
        val json = historyPath.readText()
        settings = Json.decodeFromString<AppSettings>(json)
    }

}