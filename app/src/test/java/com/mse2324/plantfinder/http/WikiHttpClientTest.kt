package com.mse2324.plantfinder.http

import kotlinx.coroutines.test.runTest

import org.junit.Test

class WikiHttpClientTest {

    @Test
    fun testPlants() = runTest {

        val c = WikiHttpClient()

        // https://en.wikipedia.org/wiki/Common_Sunflower
        val commonSunflower = c.getPlantByScientificName("helianthus annuus")
        assert(commonSunflower.extract.startsWith("The common sunflower"))
        assert(
            commonSunflower.thumbnail.source ==
            "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Sunflower_sky_backdrop.jpg/300px-Sunflower_sky_backdrop.jpg"
        )

        // https://en.wikipedia.org/wiki/Narcissus_pseudonarcissus
        val narcissusPseudonarcissus = c.getPlantByScientificName("narcissus_pseudonarcissus")
        assert(narcissusPseudonarcissus.extract.startsWith("Narcissus pseudonarcissus"))
        assert(
            narcissusPseudonarcissus.thumbnail.source ==
            "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Narcissus_pseudonarcissus_flower_300303.jpg/300px-Narcissus_pseudonarcissus_flower_300303.jpg"
        )

        // https://en.wikipedia.org/wiki/Tulipa_clusiana
        val tulipaClusiana = c.getPlantByScientificName("tulipa_clusiana")
        assert(tulipaClusiana.extract.startsWith("Tulipa clusiana, the lady tulip"))
        assert(tulipaClusiana.title == "Tulipa clusiana")
        assert(
            tulipaClusiana.thumbnail.source ==
            "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Tulip_Tulipa_clusiana_%27Lady_Jane%27_Rock_Ledge_Flower_2000px.jpg/300px-Tulip_Tulipa_clusiana_%27Lady_Jane%27_Rock_Ledge_Flower_2000px.jpg"
        )

        // has no thumbnail, should return default thumbnail
        val theSmileyMorningShow = c.getPlantByScientificName("The_smiley_morning_show")
        assert(theSmileyMorningShow.extract.startsWith("The Smiley Morning Show is a morning"))
        assert(
            theSmileyMorningShow.thumbnail.source == "" &&
            theSmileyMorningShow.thumbnail.width == -1
        )

    }
}