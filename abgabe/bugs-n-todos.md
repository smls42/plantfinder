# Bugliste der gebauten APK 
- Manchmal werden Historieneinträge nicht richtig hinzugefügt:
Nicht gefixed weil: Nicht reproduzierbar
- Auf einem Pixel 6a werden gedownloadete Bilder manchmal nicht als Option für den Upload angeboten
Nicht gefixed weil: Offizielle API auf die wir keinen Einfluss haben

# Sonstiges 
- Die App ist im Portrait-Modus gelocked, da keine Zeit für UI im Landscape Modus war
- Die Live-Erkennung funktioniert eher schlecht:
    - In examples/links.txt sind Pflanzenbilder, die durch die Live-Erkennung erfolgreich identifziert wurden
    - Am besten werden diese dafür in einem neuen Browser-Tab geöffnet.
    - Der Kopf der Pflanze sollte mit der Kamera ganz eingefangen werden 
- Die statische Erkennung mit dem Upload Button wurde mit den Bildern in examples/xxx.jpg getestet 
- Die Detailseite der Historie ist aus Zeitgründen nicht fertig geworden

# Ausblick
- Onboarding 
- Threshold Einstellung ab wann Ergebnisse der KI angezeigt werden
- Taggen von Funden mit dem aktuellen Standort und anzeigen in der Detailansicht
