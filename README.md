# Mse2324 Plantfinder 

A minimal Android-App made for Mobile Software Engineering WS23/24 to identify plants.
Images in the example directory should get correctly identified by the "Upload" function of the app.

The model for the Project can be found at: https://www.kaggle.com/models/google/aiy/frameworks/tensorFlow1/variations/vision-classifier-plants-v1
